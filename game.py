from random import randint


user_name = input("Hi! What is your name? ")

#Guess 1

for guess_number in range(1,6):
    months = randint(1 , 12)
    years = randint(1924 , 2004)

    print("Guess ", guess_number, ":", user_name, "were you born in", 
        months, "/", years )

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good Bye.")
    else: 
        
        print("Drat! Lemme try again!")

